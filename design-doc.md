# Bad Cooking
### Weekly game jam game.
___

#### Basics
- Lots of "food" items on display
- Perhaps a pizza base or a plate?
- Player puts together a dish out of those items
- Clicks button to "submit" that dish
- The dish is then "evaluated" and a score (money?) is given.

#### Evaluation
- Likely use money and have the premice being a customer asking for a dish 
and paying whatever they want for it. 
- Could be entirely random but would be interesting to try and create 
something that sees what is being used and what goes well together for 
extra points. 

#### Minimum Viable Product
- Some items on the screen
- Somewhere to put them
- Submit button that sends the dish
- Give score after submitting the dish

#### First extras
- Give a list of what was on the plate
- Give a bill based on what was put into the dish (track what items were added 
with a colision detection thing and give all items a cost)

#### Second extras
- Give score based on what was used and what goes well together
- Highscores

#### Final extras
- Make the game look all fancy and animated and shit rather than what will 
actually be the case which is very fast programmer art that *kinda* looks like 
the thing it says it is, maybe... If you squint...  

___

#### Technical Details
##### Plate
The plate will be a collision detector so that when submit is pressed I can 
see what the items on there are. This will let me give a list of items to 
the player at the end. 
##### Submit
When submit is pressed the plate will get all the items on it and list 
them. Then it will need to put them into a graphical list on screen for 
the player to see. Then the player can select to quit from there or to make 
a new dish with fresh items. 
##### Highscores
Possibly have a highscore table. It's not that hard to make, as we know 
from the skiing game, but it's just where to put the leaderboard enter 
GUI. Perhaps force the player to add a "chef name" at the start and 
automatically enter the data? 

___